<?php
include_once 'config.php';

class login_mdl extends config
{
	protected $shop_name = "";
	protected $shop_real_name = "";
	protected $token = "";
	protected $shopify_site_admin = "";
	protected $shopify_site_url_app_url = "";
	protected $shopify_web_site_url = "";
	protected $site_access_key = "";
	protected $shop_email = "";
	
	protected function getShopDetail_f_mdl()
	{
		$mysql = parent::connect();
		
		$resultArray = array();

		mysqli_set_charset($mysql, "utf8");

		if($GetResult = $mysql->query("SELECT * FROM shop_management WHERE shop_name = '".$this->shop_name."' LIMIT 1"))
		{
			while($row = mysqli_fetch_assoc($GetResult)){
				$resultArray[] = $row;
			}
			
			$GetResult->free();
		}

		parent::disconnect($mysql);

		return $resultArray;
	}
	
	protected function updateShopDetail_f_mdl()
	{
		$token = $_SESSION['token'];
		$shop = $_SESSION['shop'];
		
		$mysql = parent::connect();

		mysqli_set_charset($mysql, "utf8");

		$stmt = $mysql->prepare("UPDATE shop_management set token = ? WHERE shop_name = ?");
		
		$stmt->bind_param('ss', $token, $shop);
		
		$stmt->execute();

		$stmt->close();
		
		parent::disconnect($mysql);	
	}
	
	protected function updateShopStatus_f_mdl()
	{
		/*$status = 1;
		
		$mysql = parent::connect();

		mysqli_set_charset($mysql, "utf8");

		$stmt = $mysql->prepare("UPDATE shop_management set status = ? WHERE shop_name = ?");
		
		$stmt->bind_param('is', $status, $this->shop_name);
		
		$stmt->execute();
		$stmt->close();
		
		parent::disconnect($mysql);	*/
	}
	
	protected function updateStoreCurrency_f_mdl($shop_real_name, $email, $customer_email, $currency, $money_format, $money_with_currency_format,  $timezone, $iana_timezone, $ShopPhone, $id)
	{
		$mysql = parent::connect();
		$resultArray = array();
		mysqli_set_charset($mysql, "utf8");
		$stmt = $mysql->prepare("UPDATE shop_management SET shop_real_name = ?, email = ?, customer_email = ?, currency = ?, money_format = ?, money_with_currency_format = ?, timezone = ?, iana_timezone = ?, shop_phone = ? WHERE id = ?");
		
		$stmt->bind_param('sssssssssi',$shop_real_name, $email, $customer_email, $currency, $money_format, $money_with_currency_format, $timezone, $iana_timezone, $ShopPhone, $id);
		
		$stmt->execute();
		
		$stmt->close();
		
		parent::disconnect($mysql);	
	}

	protected function updateWebhooksIds_f_mdl($ShopId, $WebHookid, $columnName)
	{
		$mysql = parent::connect();

		$resultArray = array();

		mysqli_set_charset($mysql, "utf8");

		if($GetResult = $mysql->query("UPDATE shop_management SET ".$columnName." = '".$WebHookid."' WHERE id = '$ShopId'"))
		{
			if($GetResult) {
				$innerArray = array();
				$innerArray["class"] = "success";
				$innerArray["msg"] = "Webhook id updated";
				$resultArray[] = $innerArray;
			}
			else {
				$innerArray = array();
				$innerArray["class"] = "error";
				$innerArray["msg"] = "Oops! You have not made any changes.";
				$resultArray[] = $innerArray;
			}
		}
		
		parent::disconnect($mysql);	
	}
}
?>