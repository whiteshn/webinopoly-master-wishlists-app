<?php
include_once 'config.php';

class collection_delete_mdl extends config
{
    protected $shop_name = "";

    protected function getCollectionInfo_f_mdl($collectionId)
    {
        $mysql = parent::connect();
	
		$resultArray = array();	
	
        $stmt = $mysql->prepare("SELECT id FROM store_collections_master WHERE store_collection_id = ?");
        
        $stmt->bind_param("s", $collectionId);
	
		$stmt->execute();
	
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($id);

			while($stmt->fetch()){
				$innerArray = array();
				$innerArray["id"] = $id;
				
				$resultArray[] = $innerArray;
			}
			$stmt->free_result();
		}
	
		$stmt->close();
	
		parent::disconnect($mysql);
		
		return $resultArray;
    }

    protected function removeCollectionFromDB_f_mdl($masterCollectionId)
    {
        $mysql = parent::connect();

		$stmt = $mysql->prepare("DELETE FROM store_collections_master WHERE id = ?");
		
		$stmt->bind_param('i', $masterCollectionId);
		
		$stmt->execute();

		$stmt->close();
		
		parent::disconnect($mysql);
    }

    protected function removeCollectionFromProductBelongTBL_f_mdl($masterCollectionId)
    {
        $mysql = parent::connect();

		$stmt = $mysql->prepare("DELETE FROM store_products_belongs_collections_master WHERE store_collections_master_id = ?");
		
		$stmt->bind_param('i', $masterCollectionId);
		
		$stmt->execute();

		$stmt->close();
		
		parent::disconnect($mysql);
    }
}
?>
