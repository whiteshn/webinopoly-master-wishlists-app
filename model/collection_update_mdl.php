<?php
include_once 'config.php';

class collection_update_mdl extends config
{
    protected $shop_name = "";

    protected function updateCollection_f_mdl($collectionId, $collectionTitle, $collectionHandle)
    {
        $mysql = parent::connect();

		$stmt = $mysql->prepare("UPDATE store_collections_master set store_collection_name = ?, store_collection_handle = ? WHERE store_collection_id = ?");
		
		$stmt->bind_param('sss', $collectionTitle, $collectionHandle, $collectionId);
		
		$stmt->execute();

		$stmt->close();
		
		parent::disconnect($mysql);
    }

    protected function getCollectionInfo_f_mdl($collectionId)
    {
        $mysql = parent::connect();
	
		$resultArray = array();	
	
        $stmt = $mysql->prepare("SELECT id FROM store_collections_master WHERE store_collection_id = ?");
        
        $stmt->bind_param("s", $collectionId);
	
		$stmt->execute();
	
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($id);

			while($stmt->fetch()){
				$innerArray = array();
				$innerArray["id"] = $id;
				
				$resultArray[] = $innerArray;
			}
			$stmt->free_result();
		}
	
		$stmt->close();
	
		parent::disconnect($mysql);
		
		return $resultArray;
    }

    protected function removeCurrentCollectionsBelongsProducts_f_mdl($masterCollectionId)
    {
        $mysql = parent::connect();

        $stmt = $mysql->prepare("DELETE FROM store_products_belongs_collections_master WHERE store_collections_master_id = ?");

        $stmt->bind_param("i", $masterCollectionId);

        $stmt->execute();

        $stmt->close();

		parent::disconnect($mysql);
    }

    protected function getStoreInfo_f_mdl()
    {
        $mysql = parent::connect();
	
		$resultArray = array();	
	
		$stmt = $mysql->prepare("SELECT id, shop_name, token FROM shop_management WHERE id = 1");
	
		$stmt->execute();
	
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($id, $shop_name, $token);

			while($stmt->fetch()){
				$innerArray = array();
				$innerArray["id"] = $id;
				$innerArray["shop_name"] = $shop_name;
				$innerArray["token"] = $token;
				
				$resultArray[] = $innerArray;
			}
			$stmt->free_result();
		}
	
		$stmt->close();
	
		parent::disconnect($mysql);
		
		return $resultArray;
	}

    protected function getStoreAllProducts_f_mdl()
    {
        $mysql = parent::connect();
	
		$resultArray = array();	
	
		$stmt = $mysql->prepare("SELECT id as master_product_id, store_product_id FROM store_products_master");
	
		$stmt->execute();
	
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($master_product_id, $store_product_id);

			while($stmt->fetch()){
				$innerArray = array();
				$innerArray["master_product_id"] = $master_product_id;
				$innerArray["store_product_id"] = $store_product_id;
				
				$resultArray[] = $innerArray;
			}
			$stmt->free_result();
		}
	
		$stmt->close();
	
		parent::disconnect($mysql);
		
		return $resultArray;
    }

    protected function insertBulkProductBelongsCollections_f_mdl($bulkIns)
	{
		$mysql = parent::connect();
		
		$resultArray = array();	

		$stmt = $mysql->prepare("INSERT INTO store_products_belongs_collections_master(store_collections_master_id, store_products_master_id, created_on) VALUES".$bulkIns);
		
		$stmt->execute();
		
		parent::disconnect($mysql);
    }
    
    protected function insertDummyVal_f_mdl($data)
    {
        $mysql = parent::connect();

        mysqli_set_charset($mysql, "utf8");

        $stmt = $mysql->prepare("INSERT INTO test_table(test_val) VALUES(?)");

        $stmt->bind_param("s", $data);

        $stmt->execute();

        $stmt->close();

		parent::disconnect($mysql);
    }
}
?>
