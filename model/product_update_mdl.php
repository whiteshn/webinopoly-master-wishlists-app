<?php
include_once 'config.php';

class product_update_mdl extends config
{
    protected $shop_name = "";

    protected function updateMasterProduct_f_mdl($storeProductId, $storeProductTitle, $storeProductHandle, $storeProductFeatureImg)
	{
		$mysql = parent::connect();
		
		$stmt = $mysql->prepare("UPDATE store_products_master SET store_product_title = ?, store_product_handle = ?, store_product_feature_img = ? WHERE store_product_id = ?");
		
		$stmt->bind_param("ssss", $storeProductTitle, $storeProductHandle, $storeProductFeatureImg, $storeProductId);
		
		$stmt->execute();
		
		parent::disconnect($mysql);	
    }

    protected function getMasterProductIdAndDeleteVariants_f_mdl($store_product_id)
    {
        $mysql = parent::connect();

        #region - Get Master Product Id
        $resultArray = array();

        $stmt = $mysql->prepare("SELECT id FROM store_products_master WHERE store_product_id = ?");

        $stmt->bind_param("s", $store_product_id);
        
        $stmt->execute();
		
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($id);

			while($stmt->fetch()){
				$innerArray = array();
                $innerArray["id"] = $id;
                
				$resultArray[] = $innerArray;
			}
			$stmt->free_result();
		}
	
		$stmt->close();
        #endregion

        $masterProductId = 0;

        if(count($resultArray) > 0){
            $masterProductId = $resultArray[0]["id"];

            #region - Delete Product Variants
            /*$stmt = $mysql->prepare("DELETE FROM store_products_variants_master WHERE store_products_master_id = ?");

            $stmt->bind_param("i", $masterProductId);

            $stmt->execute();

            $stmt->close();*/
            #endregion
        }

        parent::disconnect($mysql);

        return $masterProductId;
    }
    
    protected function bulkProductVariantsInsert_f_mdl($variantsBulkIns)
	{
		$mysql = parent::connect();
		
		$resultArray = array();	

		$stmt = $mysql->prepare("INSERT INTO store_products_variants_master(store_products_master_id, store_product_var_id, store_product_var_title, store_product_var_sku, store_product_var_img, store_product_var_price, created_on) VALUES".$variantsBulkIns);
		
		$stmt->execute();
		
		parent::disconnect($mysql);
	}
    
    protected function insertDummyVal_f_mdl($data)
    {
        $mysql = parent::connect();

        mysqli_set_charset($mysql, "utf8");

        $stmt = $mysql->prepare("INSERT INTO test_table(test_val) VALUES(?)");

        $stmt->bind_param("s", $data);

        $stmt->execute();

        $stmt->close();

		parent::disconnect($mysql);
    }

    protected function updateProductVariantsInfo_f_mdl($productMasterId, $storeVariantId, $storeVarianttitle, $storeVariantsku, $storeVariantImg, $storeVariantprice)
    {
		$mysql = parent::connect();
		
		#region - Check Variant Exist Or Not
		$resultArray = array();

        $stmt = $mysql->prepare("SELECT id FROM store_products_variants_master WHERE store_product_var_id = ? AND store_products_master_id = ?");

        $stmt->bind_param("si", $storeVariantId, $productMasterId);
        
        $stmt->execute();
		
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($id);

			while($stmt->fetch()){
				$innerArray = array();
                $innerArray["id"] = $id;
                
				$resultArray[] = $innerArray;
			}
			$stmt->free_result();
		}
	
		$stmt->close();
		#endregion

		if(count($resultArray) > 0){
			#region - Update Current Exist Variant
			$stmt = $mysql->prepare("UPDATE store_products_variants_master SET store_product_var_title = ?, store_product_var_sku = ?, store_product_var_img = ?, store_product_var_price = ? WHERE store_product_var_id = ? AND store_products_master_id = ?");
		
			$stmt->bind_param("sssssi", $storeVarianttitle, $storeVariantsku, $storeVariantImg, $storeVariantprice, $storeVariantId, $productMasterId);
			
			$stmt->execute();
			#endregion
		}
		else{
			#region - Insert New Product Variant
			$stmt = $mysql->prepare("INSERT INTO store_products_variants_master(store_products_master_id, store_product_var_id, store_product_var_title, store_product_var_sku, store_product_var_img, store_product_var_price, created_on) VALUES(?, ?, ?, ?, ?, ?, now())");

			$stmt->bind_param("isssss", $productMasterId, $storeVariantId, $storeVarianttitle, $storeVariantsku, $storeVariantImg, $storeVariantprice);
		
			$stmt->execute();
			#endregion
		}

		$stmt->close();
		
		parent::disconnect($mysql);	
    }

    protected function getStoreInfo_f_mdl()
    {
        $mysql = parent::connect();
	
		$resultArray = array();	
	
		$stmt = $mysql->prepare("SELECT id, shop_name, token FROM shop_management WHERE id = 1");
	
		$stmt->execute();
	
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($id, $shop_name, $token);

			while($stmt->fetch()){
				$innerArray = array();
				$innerArray["id"] = $id;
				$innerArray["shop_name"] = $shop_name;
				$innerArray["token"] = $token;
				
				$resultArray[] = $innerArray;
			}
			$stmt->free_result();
		}
	
		$stmt->close();
	
		parent::disconnect($mysql);
		
		return $resultArray;
	}

	protected function getStoreAllCollections_f_mdl()
    {
        $mysql = parent::connect();
	
		$resultArray = array();	
	
		$stmt = $mysql->prepare("SELECT id as master_collection_id, store_collection_id FROM store_collections_master");
	
		$stmt->execute();
	
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($master_collection_id, $store_collection_id);

			while($stmt->fetch()){
				$innerArray = array();
				$innerArray["master_collection_id"] = $master_collection_id;
				$innerArray["store_collection_id"] = $store_collection_id;
				
				$resultArray[] = $innerArray;
			}
			$stmt->free_result();
		}
	
		$stmt->close();
	
		parent::disconnect($mysql);
		
		return $resultArray;
	}
	
	protected function insertBulkProductBelongsCollections_f_mdl($bulkIns)
	{
		$mysql = parent::connect();
		
		$resultArray = array();	

		$stmt = $mysql->prepare("INSERT INTO store_products_belongs_collections_master(store_collections_master_id, store_products_master_id, created_on) VALUES".$bulkIns);
		
		$stmt->execute();
		
		parent::disconnect($mysql);
    }
    
    protected function removeProductBelongsCollectionsRows_f_mdl($dbMasterProductId)
    {
        $mysql = parent::connect();

        $stmt = $mysql->prepare("DELETE FROM store_products_belongs_collections_master WHERE store_products_master_id = ?");

        $stmt->bind_param("i", $dbMasterProductId);

        $stmt->execute();

        $stmt->close();

        parent::disconnect($mysql);
    }
}
?>
