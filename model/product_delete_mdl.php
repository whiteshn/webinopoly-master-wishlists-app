<?php
include_once 'config.php';

class product_delete_mdl extends config
{
    protected $shop_name = "";

    protected function getProductDBInfo_f_mdl($shopifyProductId)
    {
        $mysql = parent::connect();

        $resultArray = array();

        $stmt = $mysql->prepare("SELECT id FROM store_products_master WHERE store_product_id = ?");

        $stmt->bind_param("s", $shopifyProductId);
        
        $stmt->execute();
		
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($id);

			while($stmt->fetch()){
				$innerArray = array();
                $innerArray["id"] = $id;
                
				$resultArray[] = $innerArray;
			}
			$stmt->free_result();
		}
	
		$stmt->close();
        
        parent::disconnect($mysql);

        return $resultArray;
    }

    protected function deleteProductFromEverywhere_f_mdl($masterProductId)
    {
        $mysql = parent::connect();

        #region - Delete Master Product
        $stmt = $mysql->prepare("DELETE FROM store_products_master WHERE id = ?");

        $stmt->bind_param("i", $masterProductId);

        $stmt->execute();

        $stmt->close();
        #endregion

        #region - Delete Product Variants
        $stmt = $mysql->prepare("DELETE FROM store_products_variants_master WHERE store_products_master_id = ?");

        $stmt->bind_param("i", $masterProductId);

        $stmt->execute();

        $stmt->close();
        #endregion

        #region - Delete Product From Customer's Wishlist
        $stmt = $mysql->prepare("DELETE FROM customer_selected_products_master WHERE store_products_master_id = ?");

        $stmt->bind_param("i", $masterProductId);

        $stmt->execute();

        $stmt->close();
        #endregion

        #region - Delete Product From Collection Belongs Table
        $stmt = $mysql->prepare("DELETE FROM store_products_belongs_collections_master WHERE store_products_master_id = ?");

        $stmt->bind_param("i", $masterProductId);

        $stmt->execute();

        $stmt->close();
        #endregion

		parent::disconnect($mysql);
    }
}
?>
