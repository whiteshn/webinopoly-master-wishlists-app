<?php
session_start();
ob_start();

include_once 'common.php';

class config extends common
{
	private $hostName = common::DB_HOST_NAME;
	private $dbName = common::DB_NAME;
	private $userName = common::DB_USERNAME;
	private $password = common::DB_PASSWORD;
	
	protected function connect()
	{
		return mysqli_connect($this->hostName, $this->userName, $this->password, $this->dbName);
	}	
	
	protected function disconnect($link)
	{
		$link->close();
	}
	
	protected function dummy_insert($val)
	{
		$mysql = $this->connect();
		
		$resultArray = array();	
		
		$stmt = $mysql->prepare("INSERT INTO test_table(val) VALUES(?)");
		
		$stmt->bind_param("s", $val);
		
		$stmt->execute();
		
		$this->disconnect($mysql);
	}
}
?>