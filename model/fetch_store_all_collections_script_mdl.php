<?php
include_once 'config.php';

class fetch_store_all_collections_script_mdl extends config
{

    protected function getStoreInfo_f_mdl()
    {
        $mysql = parent::connect();
	
		$resultArray = array();	
	
		$stmt = $mysql->prepare("SELECT id, shop_name, token FROM shop_management WHERE id = 1");
	
		$stmt->execute();
	
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($id, $shop_name, $token);

			while($stmt->fetch()){
				$innerArray = array();
				$innerArray["id"] = $id;
				$innerArray["shop_name"] = $shop_name;
				$innerArray["token"] = $token;
				
				$resultArray[] = $innerArray;
			}
			$stmt->free_result();
		}
	
		$stmt->close();
	
		parent::disconnect($mysql);
		
		return $resultArray;
    }
    
    protected function addStoreCollections_f_mdl($bulkInsertStr)
    {
        $mysql = parent::connect();
		
		$stmt = $mysql->prepare("INSERT INTO store_collections_master(store_collection_id, store_collection_name, store_collection_handle, created_on) VALUES".$bulkInsertStr);
		
		$stmt->execute();
		
		parent::disconnect($mysql);
    }
}
?>