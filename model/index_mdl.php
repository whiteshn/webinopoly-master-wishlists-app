<?php
include_once 'config.php';

class index_mdl extends config
{
	protected $shop_name = "";
	
	protected function getShopDetail_f_mdl()
	{
		$mysql = parent::connect();
	
		$resultArray = array();	
	
		$stmt = $mysql->prepare("SELECT id, shop_name, token, shopify_site_admin, shopify_site_url_app_url, shopify_web_site_url, currency, money_format, money_with_currency_format, site_access_key from shop_management WHERE shop_name = ?");
	
		$stmt->bind_param('s', $this->shop_name);
		
		$stmt->execute();
	
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($id, $shop_name, $token, $shopify_site_admin, $shopify_site_url_app_url, $shopify_web_site_url, $currency, $money_format, $money_with_currency_format, $site_access_key);

			while($stmt->fetch()){
				$innerArray = array();
				$innerArray["id"] = $id;
				$innerArray["shop_name"] = $shop_name;
				$innerArray["token"] = $token;
				$innerArray["shopify_site_admin"] = $shopify_site_admin;
				$innerArray["shopify_site_url_app_url"] = $shopify_site_url_app_url;
				$innerArray["shopify_web_site_url"] = $shopify_web_site_url;
				$innerArray["currency"] = $currency;
				$innerArray["money_format"] = $money_format;
				$innerArray["money_with_currency_format"] = $money_with_currency_format;
				$innerArray["site_access_key"] = $site_access_key;
				
				$resultArray[] = $innerArray;
			}
			$stmt->free_result();
		}
	
		$stmt->close();
	
		parent::disconnect($mysql);
		
		return $resultArray;
	}
}
?>