<?php
include_once 'config.php';

class process_on_request_mdl extends config
{
	public $customer_id = 0;
	public $store_product_id = "";
	public $master_product_id = 0;
	public $customer_selected_master_id = 0;
	public $master_collections_id = 0;

	protected function getCustomerSelectedProductsShortInfo_f_mdl()
	{
		$mysql = parent::connect();
		
		$resultArray = array();	
		
		$stmt = $mysql->prepare("SELECT spm.store_product_id FROM customer_selected_products_master cspm INNER JOIN store_products_master spm ON cspm.store_products_master_id = spm.id WHERE cspm.shopify_customer_id = ?");

		$stmt->bind_param("s", $this->customer_id);
		
		$stmt->execute();
		
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($store_product_id);

			while($stmt->fetch()){
				$innerArray = array();
				$innerArray["store_product_id"] = $store_product_id;

				$resultArray[] = $innerArray;
            }

			$stmt->free_result();
		}
	
		$stmt->close();
		
		parent::disconnect($mysql);
		
		common::sendJson($resultArray);
	}

	protected function getMasterProductInfo_f_mdl()
	{
		$mysql = parent::connect();
		
		$resultArray = array();	
		
		$stmt = $mysql->prepare("SELECT spm.id as master_products_id, spvm.id as master_products_variants_id FROM store_products_master spm INNER JOIN store_products_variants_master spvm ON spm.id = spvm.store_products_master_id WHERE spm.store_product_id = ?");

		$stmt->bind_param("s", $this->store_product_id);
		
		$stmt->execute();
		
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($master_products_id, $master_products_variants_id);

			while($stmt->fetch()){
				$innerArray = array();
				$innerArray["master_products_id"] = $master_products_id;
				$innerArray["master_products_variants_id"] = $master_products_variants_id;

				$resultArray[] = $innerArray;
            }

			$stmt->free_result();
		}
	
		$stmt->close();
		
		parent::disconnect($mysql);
		
		return $resultArray;
	}

	protected function addCurrentProductToWishlistItem_f_mdl($bulkInsStr)
	{
		$mysql = parent::connect();

		$resultArray = array();	
	
		$stmt = $mysql->prepare("INSERT INTO customer_selected_products_master(shopify_customer_id, store_products_master_id, store_products_variants_master_id, created_on) VALUES".$bulkInsStr);

		$stmt->execute();

		$stmt->free_result();
		
		if($mysql->insert_id > 0) {
			$resultArray["isSuccess"] = "1";
			$resultArray["msg"] = "Product was successfully added to Wishlist.";
		}
		else {
			$resultArray["isSuccess"] = "0";
			$resultArray["msg"] = "Oops! There was some issues. Please try again after sometime or refresh page.";
		}
	
		$stmt->close();
	
		parent::disconnect($mysql);
		
		common::sendJson($resultArray);
	}

	protected function getCustomerItemsForList_f_mdl()
	{
		$mysql = parent::connect();
		
		$resultArray = array();	
		
		$stmt = $mysql->prepare("SELECT distinct cspm.id as c_master_id, spm.id as m_prod_id, spm.store_product_id, spm.store_product_title, spm.store_product_feature_img, spvm.store_product_var_id, spvm.store_product_var_title, spvm.store_product_var_sku, spvm.store_product_var_img, spvm.store_product_var_price, scm.store_collection_name FROM customer_selected_products_master cspm INNER JOIN store_products_master spm ON cspm.store_products_master_id = spm.id INNER JOIN store_products_variants_master spvm ON cspm.store_products_variants_master_id = spvm.id INNER JOIN store_products_belongs_collections_master spbcm ON spm.id = spbcm.store_products_master_id INNER JOIN store_collections_master scm ON spbcm.store_collections_master_id = scm.id WHERE cspm.shopify_customer_id = ? ORDER BY spbcm.store_collections_master_id, spm.store_product_title");

		$stmt->bind_param("s", $this->customer_id);
		
		$stmt->execute();
		
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($c_master_id, $m_prod_id, $store_product_id, $store_product_title, $store_product_feature_img, $store_product_var_id, $store_product_var_title, $store_product_var_sku, $store_product_var_img, $store_product_var_price, $store_collection_name);

			while($stmt->fetch()){
				$innerArray = array();
				$innerArray["c_master_id"] = $c_master_id;
				$innerArray["m_prod_id"] = $m_prod_id;
				$innerArray["store_product_id"] = $store_product_id;
				$innerArray["store_product_title"] = $store_product_title;
				$innerArray["store_product_feature_img"] = $store_product_feature_img;
				$innerArray["store_product_var_id"] = $store_product_var_id;
				$innerArray["store_product_var_title"] = $store_product_var_title;
				$innerArray["store_product_var_sku"] = $store_product_var_sku;
				$innerArray["store_product_var_img"] = $store_product_var_img;
				$innerArray["store_product_var_price"] = $store_product_var_price;
				$innerArray["store_collection_name"] = $store_collection_name;

				$resultArray[] = $innerArray;
            }

			$stmt->free_result();
		}
	
		$stmt->close();
		
		parent::disconnect($mysql);
		
		common::sendJson($resultArray);
	}

	protected function removeProductFromWishist_f_mld()
	{
		$mysql = parent::connect();

		$resultArray = array();

		if($GetResult = $mysql->query("DELETE FROM customer_selected_products_master WHERE id = ".$this->customer_selected_master_id." AND shopify_customer_id = '".$this->customer_id."'"))
		{
			if($GetResult) {
				$resultArray["isSuccess"] = "1";
				$resultArray["msg"] = "Product removed successfully";
			}
			else {
				$resultArray["isSuccess"] = "0";
				$resultArray["msg"] = "Oops! there is some issue while removing product from list. Please try after some time.";
			}
		}
		
		parent::disconnect($mysql);

		common::sendJson($resultArray);
	}

	protected function getCollectionMasterInfo_f_mdl($requestCollectionId)
	{
		$mysql = parent::connect();
		
		$resultArray = array();	
		
		$stmt = $mysql->prepare("SELECT id FROM store_collections_master WHERE store_collection_id = ?");

		$stmt->bind_param("s", $requestCollectionId);
		
		$stmt->execute();
		
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($id);

			while($stmt->fetch()){
				$innerArray = array();
				$innerArray["id"] = $id;

				$resultArray[] = $innerArray;
            }

			$stmt->free_result();
		}
	
		$stmt->close();
		
		parent::disconnect($mysql);
		
		return $resultArray;
	}

	protected function getCustomerItemsForListCollectWise_f_mdl()
	{
		$mysql = parent::connect();
		
		$resultArray = array();	
		
		$stmt = $mysql->prepare("SELECT cspm.id as c_master_id, spm.id as m_prod_id, spm.store_product_id, spm.store_product_title, spm.store_product_feature_img, spvm.store_product_var_id, spvm.store_product_var_title, spvm.store_product_var_sku, spvm.store_product_var_img, spvm.store_product_var_price, scm.store_collection_name FROM customer_selected_products_master cspm INNER JOIN store_products_master spm ON cspm.store_products_master_id = spm.id INNER JOIN store_products_variants_master spvm ON cspm.store_products_variants_master_id = spvm.id INNER JOIN store_products_belongs_collections_master spbcm ON spm.id = spbcm.store_products_master_id AND spbcm.store_collections_master_id = ? INNER JOIN store_collections_master scm ON spbcm.store_collections_master_id = scm.id WHERE cspm.shopify_customer_id = ? ORDER BY spm.store_product_title");

		$stmt->bind_param("is", $this->master_collections_id, $this->customer_id);
		
		$stmt->execute();
		
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($c_master_id, $m_prod_id, $store_product_id, $store_product_title, $store_product_feature_img, $store_product_var_id, $store_product_var_title, $store_product_var_sku, $store_product_var_img, $store_product_var_price, $store_collection_name);

			while($stmt->fetch()){
				$innerArray = array();
				$innerArray["c_master_id"] = $c_master_id;
				$innerArray["m_prod_id"] = $m_prod_id;
				$innerArray["store_product_id"] = $store_product_id;
				$innerArray["store_product_title"] = $store_product_title;
				$innerArray["store_product_feature_img"] = $store_product_feature_img;
				$innerArray["store_product_var_id"] = $store_product_var_id;
				$innerArray["store_product_var_title"] = $store_product_var_title;
				$innerArray["store_product_var_sku"] = $store_product_var_sku;
				$innerArray["store_product_var_img"] = $store_product_var_img;
				$innerArray["store_product_var_price"] = $store_product_var_price;
				$innerArray["store_collection_name"] = $store_collection_name;

				$resultArray[] = $innerArray;
            }

			$stmt->free_result();
		}
	
		$stmt->close();
		
		parent::disconnect($mysql);
		
		common::sendJson($resultArray);
	}
}
?>
