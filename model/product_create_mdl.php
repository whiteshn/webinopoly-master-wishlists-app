<?php
include_once 'config.php';

class product_create_mdl extends config
{
    protected $shop_name = "";

    protected function addMasterProduct_f_mdl($storeProductId, $storeProductTitle, $storeProductHandle, $storeProductFeatureImg)
	{
		$mysql = parent::connect();
		
		$resultArray = array();	

		$stmt = $mysql->prepare("INSERT INTO store_products_master(store_product_id, store_product_title, store_product_handle, store_product_feature_img, created_on) VALUES(?, ?, ?, ?, now())");
		
		$stmt->bind_param("ssss", $storeProductId, $storeProductTitle, $storeProductHandle, $storeProductFeatureImg);
		
		$stmt->execute();
		
		$insertedId = $mysql->insert_id;
		
		parent::disconnect($mysql);	
		
		return $insertedId;
    }
    
    protected function bulkProductVariantsInsert_f_mdl($variantsBulkIns)
	{
		$mysql = parent::connect();
		
		$resultArray = array();	

		$stmt = $mysql->prepare("INSERT INTO store_products_variants_master(store_products_master_id, store_product_var_id, store_product_var_title, store_product_var_sku, store_product_var_img, store_product_var_price, created_on) VALUES".$variantsBulkIns);
		
		$stmt->execute();
		
		parent::disconnect($mysql);
	}
    
    protected function insertDummyVal_f_mdl($data)
    {
        $mysql = parent::connect();

        mysqli_set_charset($mysql, "utf8");

        $stmt = $mysql->prepare("INSERT INTO test_table(test_val) VALUES(?)");

        $stmt->bind_param("s", $data);

        $stmt->execute();

        $stmt->close();

		parent::disconnect($mysql);
	}
	
	protected function getStoreInfo_f_mdl()
    {
        $mysql = parent::connect();
	
		$resultArray = array();	
	
		$stmt = $mysql->prepare("SELECT id, shop_name, token FROM shop_management WHERE id = 1");
	
		$stmt->execute();
	
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($id, $shop_name, $token);

			while($stmt->fetch()){
				$innerArray = array();
				$innerArray["id"] = $id;
				$innerArray["shop_name"] = $shop_name;
				$innerArray["token"] = $token;
				
				$resultArray[] = $innerArray;
			}
			$stmt->free_result();
		}
	
		$stmt->close();
	
		parent::disconnect($mysql);
		
		return $resultArray;
	}

	protected function getStoreAllCollections_f_mdl()
    {
        $mysql = parent::connect();
	
		$resultArray = array();	
	
		$stmt = $mysql->prepare("SELECT id as master_collection_id, store_collection_id FROM store_collections_master");
	
		$stmt->execute();
	
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($master_collection_id, $store_collection_id);

			while($stmt->fetch()){
				$innerArray = array();
				$innerArray["master_collection_id"] = $master_collection_id;
				$innerArray["store_collection_id"] = $store_collection_id;
				
				$resultArray[] = $innerArray;
			}
			$stmt->free_result();
		}
	
		$stmt->close();
	
		parent::disconnect($mysql);
		
		return $resultArray;
	}
	
	protected function insertBulkProductBelongsCollections_f_mdl($bulkIns)
	{
		$mysql = parent::connect();
		
		$resultArray = array();	

		$stmt = $mysql->prepare("INSERT INTO store_products_belongs_collections_master(store_collections_master_id, store_products_master_id, created_on) VALUES".$bulkIns);
		
		$stmt->execute();
		
		parent::disconnect($mysql);
	}
}
?>
