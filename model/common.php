<?php
class common
{
	public $SHOP_ID = "";
	public $SHOPIFY_LOGIN_URL = "";
	public $SHOPIFY_SITE = "";
	public $SHOPIFY_SITE_URL = "";
	public $SHOPIFY_WEB_SITE_URL = "";
	public $SITE_ACCESS_KEY = "";
	public $TOKEN = "";
	public $EMAIL = "";
	public $SHOP_REAL_NAME = "";
	public $USER_TYPE = "";

	public $byPassFunction = false;
	public $loginPage = false;

	public $isRoot = true;

	const IS_LIVE = true;

	#region - Connection Variables
	const DB_HOST_NAME = "localhost";
	const DB_NAME = "master_wishlists_app";
	const DB_USERNAME = "root";
	const DB_PASSWORD = "";
	#endregion

	#region - Shopify Variables
	const SHOPIFY_API_KEY = "6583daa417eb8e65c8d8a40e64b926c2";
	const SHOPIFY_SECRET = "e518762c6f7e0b144605c9f292572bdb";
	const SHOPIFY_SCOPE = "write_products";
	#endregion

	#region - Configuration Site Variables
	const SITE_URL = "https://8f0de9d0.ngrok.io/master-wishlists-app-local/";
	const SITE_URL_FLAXIBLE = "//8f0de9d0.ngrok.io/master-wishlists-app-local/";
	const STATUS_SITE_URL = "https://status.shopify.com/";
	const SITE_NAME = 'Master Wishlist\'s App';
	const APP_SUB_FOLDER = '/apps/master-wishlists-app/';
	const PARENT_STORE_NAME = 'demo-master-wishlist-app-store.myshopify.com';

	const SHOPIFY_COLLECTION_CREATE_WEBHOOK = 'https://8f0de9d0.ngrok.io/master-wishlists-app-local/collection-create.php';
	const SHOPIFY_COLLECTION_UPDATE_WEBHOOK = 'https://8f0de9d0.ngrok.io/master-wishlists-app-local/collection-update.php';
	const SHOPIFY_COLLECTION_DELETE_WEBHOOK = 'https://8f0de9d0.ngrok.io/master-wishlists-app-local/collection-delete.php';
	const SHOPIFY_PRODUCT_CREATE_WEBHOOK = 'https://8f0de9d0.ngrok.io/master-wishlists-app-local/product-create.php';
	const SHOPIFY_PRODUCT_UPDATE_WEBHOOK = 'https://8f0de9d0.ngrok.io/master-wishlists-app-local/product-update.php';
	const SHOPIFY_PRODUCT_DELETE_WEBHOOK = 'https://8f0de9d0.ngrok.io/master-wishlists-app-local/product-delete.php';
	#endregion

	#region - Shopify CDN For Faster Load
	/* ADMIN FILES START */
	const APP_ADMIN_SHOPIFY_CDN_CSS_URL = 'https://8f0de9d0.ngrok.io/master-wishlists-app-local/css/admin-app.css?v=';
	/* ADMIN FILES END */

	/* FRONT FILES START */
	const SHOPIFY_APP_CSS_URL = 'https://8f0de9d0.ngrok.io/master-wishlists-app-local/app/css/app.css?v=1';
	/* FRONT FILES END */
	#endregion

	const IMG_URL = 'https://8f0de9d0.ngrok.io/master-wishlists-app-local/image/';
	const SHOPIFY_DEFAULT_IMAGE = '';

	#region - Cron Job Token
	const CRN_TOKEN = "9ff677695137a162b91ca07f2c4f29c694830e28";
	#endregion

	#region - Bundle.js Tunnel
	//const BUNDLE_JS_URL = "https://8f0de9d0.ngrok.io/master-wishlists-app-local/js/bundle.js?v=1";
	const BUNDLE_JS_URL = "https://e0410b92.ngrok.io/bundle.js";
	#endregion

	#region - Store Session Name
	const STORE_LOGIN_SESSION_FIRST_KEY = "storeInfo_";
	const TEMP_STORE_SESSION_FOR_PLANS = "temp_store_session";
	const TEMP_STORE_SESSION_PLANS_INFO = "temp_store_plan_info_";
	const TEMP_STORE_SESSION_FOR_PLANS_INFO = "temp_store_plan_info";
	#endregion

	public function CheckLoginSession()
	{
		$LoginSessionName = self::STORE_LOGIN_SESSION_FIRST_KEY.$this->SITE_ACCESS_KEY;
		if(isset($_SESSION[$LoginSessionName])) {
			$saveStoreInfo = json_decode($_SESSION[$LoginSessionName], true);

			if(is_array($saveStoreInfo)) {
				$this->SHOP_ID = $saveStoreInfo["SHOP_ID"];
				$this->SHOPIFY_LOGIN_URL = $saveStoreInfo["SHOPIFY_LOGIN_URL"];
				$this->SHOPIFY_SITE = $saveStoreInfo["SHOPIFY_SITE"];
				$this->SHOPIFY_SITE_URL = str_replace("http://", "https://", $saveStoreInfo["SHOPIFY_SITE_URL"]);
				$this->SHOPIFY_WEB_SITE_URL = $saveStoreInfo["SHOPIFY_WEB_SITE_URL"];
				$this->SITE_ACCESS_KEY = $saveStoreInfo["SITE_ACCESS_KEY"];
				$this->TOKEN = $saveStoreInfo["TOKEN"];
				$this->EMAIL = $saveStoreInfo["EMAIL"];
			}
			else {
				if(!$this->loginPage){
					$queryString = http_build_query($_GET);
					header("location:login.php?".$queryString);
					exit;
				}
			}
		}
		else {
			if($this->isGET()) {
				if($this->getVal("hmac") != "" && $this->getVal("shop") != "" && !$this->loginPage){
					header("location:".self::SITE_URL."login.php?shop=".$this->getVal("shop"));
					exit;
				}
			}
			else if(!$this->loginPage) {
				header("location:".self::SITE_URL."logout.php");
				exit;
			}
		}

		$this->page = substr(basename($_SERVER['PHP_SELF']),0,-4);
	}

	public function getStoreName()
	{
		return $this->SHOPIFY_WEB_SITE_URL;
	}

	public static function getPageName()
	{
		return substr(basename($_SERVER['PHP_SELF']),0,-4);
	}

	public function getStoreURL()
	{
		$saveStoreInfo = json_decode($_SESSION["storeInfo"], true);
		if(is_array($saveStoreInfo)) {
			return $saveStoreInfo["SHOPIFY_WEB_SITE_URL"];
		}
		else
			return "";
	}

	public function getVal($value)
	{
		return isset($_REQUEST[$value]) ? $_REQUEST[$value] : 0;
	}

	public function isGET()
	{
		return $_GET ? true : false;
	}

	public function isPOST()
	{
		return $_POST ? true : false;
	}

	public function isLogin()
	{
		if(empty($_SESSION['token']))
			return false;
		else
			return true;
	}

	public function set_date($datetime)
	{
		date_default_timezone_set('UTC');
		$date = array();
		$apt_time = date("h:i A", strtotime($datetime));
		$apt_date = date("M jS, Y", strtotime($datetime));
		$date["msg_date"] = $apt_date;
		$date["msg_time"] = $apt_time;
		$date["msg_datetime"] = date("M jS, Y h:i A", strtotime($datetime));
		return $date;
	}

	public static function GUID()
	{
		if (function_exists('com_create_guid') === true)
			return trim(com_create_guid(), '{}');

		return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
	}

	function get_client_ip()
	{
		$ipaddress = '';
		if (isset($_SERVER['HTTP_CLIENT_IP']))
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_X_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		else if(isset($_SERVER['REMOTE_ADDR']))
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}

	function getSession($sessionName)
	{
		return $_SESSION[$sessionName];
	}

	public function setSession($sessionName, $sessionValue)
	{
		$_SESSION[$sessionName] = $sessionValue;
	}

	public static function sendJson($obj, $isResponseEnd = true)
	{
		header('Content-Type: application/json');
		echo json_encode($obj);
		if($isResponseEnd)
			exit;
	}
}
?>