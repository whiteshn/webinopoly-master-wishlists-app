<?php
include_once 'config.php';

class fetch_store_all_products_script_mdl extends config
{

    protected function getStoreInfo_f_mdl()
    {
        $mysql = parent::connect();
	
		$resultArray = array();	
	
		$stmt = $mysql->prepare("SELECT id, shop_name, token FROM shop_management WHERE id = 1");
	
		$stmt->execute();
	
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($id, $shop_name, $token);

			while($stmt->fetch()){
				$innerArray = array();
				$innerArray["id"] = $id;
				$innerArray["shop_name"] = $shop_name;
				$innerArray["token"] = $token;
				
				$resultArray[] = $innerArray;
			}
			$stmt->free_result();
		}
	
		$stmt->close();
	
		parent::disconnect($mysql);
		
		return $resultArray;
	}
	
	protected function addMasterProduct_f_mdl($storeProductId, $storeProductTitle, $storeProductHandle, $storeProductFeatureImg)
	{
		$mysql = parent::connect();
		
		$resultArray = array();	

		$stmt = $mysql->prepare("INSERT INTO store_products_master(store_product_id, store_product_title, store_product_handle, store_product_feature_img, created_on) VALUES(?, ?, ?, ?, now())");
		
		$stmt->bind_param("ssss", $storeProductId, $storeProductTitle, $storeProductHandle, $storeProductFeatureImg);
		
		$stmt->execute();
		
		$insertedId = $mysql->insert_id;
		
		parent::disconnect($mysql);	
		
		return $insertedId;
	}

	protected function bulkProductVariantsInsert_f_mdl($variantsBulkIns)
	{
		$mysql = parent::connect();
		
		$resultArray = array();	

		$stmt = $mysql->prepare("INSERT INTO store_products_variants_master(store_products_master_id, store_product_var_id, store_product_var_title, store_product_var_sku, store_product_var_img, store_product_var_price, created_on) VALUES".$variantsBulkIns);
		
		$stmt->execute();
		
		parent::disconnect($mysql);
	}

	protected function removeProductsInfoFromDB_f_mdl()
	{
		$mysql = parent::connect();

		#region - Truncate Products Master Table
		$stmt = $mysql->prepare("TRUNCATE TABLE store_products_master");
		
		$stmt->execute();
		#endregion

		#region - Truncate Products Variants Table
		$stmt = $mysql->prepare("TRUNCATE TABLE store_products_variants_master");
		
		$stmt->execute();
		#endregion
	}
}
?>