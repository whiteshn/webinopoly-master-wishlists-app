<?php
include_once 'config.php';

class dashboard_mdl extends config
{
    protected function getStoreAllProductsFromDB_f_mdl()
    {
        $mysql = parent::connect();
		
		$resultArray = array();	
		
		$stmt = $mysql->prepare("SELECT id as products_master_id, product_id, product_title, product_handle, product_image, (SELECT count(distinct olim.orders_master_id) FROM orders_line_items_master olim INNER JOIN orders_master om ON olim.orders_master_id = om.id WHERE om.is_order_refunded = 0 AND olim.variant_product_id = pm.product_id) product_orders_count FROM products_master pm ORDER BY product_title");
		
		$stmt->execute();
		
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($products_master_id, $product_id, $product_title, $product_handle, $product_image, $product_orders_count);

			while($stmt->fetch()){
				$innerArray = array();
				$innerArray["products_master_id"] = $products_master_id;
                $innerArray["product_id"] = $product_id;
                $innerArray["product_title"] = $product_title;
                $innerArray["product_handle"] = $product_handle;
				$innerArray["product_image"] = $product_image;
				$innerArray["product_orders_count"] = $product_orders_count;
				
				$resultArray[] = $innerArray;
			}
			$stmt->free_result();
		}
	
		$stmt->close();
		
		parent::disconnect($mysql);
		
		return $resultArray;
	}
	
	protected function searchSpecificProduct_f_mdl($searchFor)
	{
		$mysql = parent::connect();
		
		$resultArray = array();	
		
		$stmt = $mysql->prepare("SELECT distinct products_master_id, product_id, product_title, product_handle, product_image, product_orders_count FROM (SELECT pm.id as products_master_id, pm.product_id, pm.product_title, pm.product_handle, pm.product_image, (SELECT count(distinct olim.orders_master_id) FROM orders_line_items_master olim WHERE olim.variant_product_id = pm.product_id) product_orders_count FROM products_master pm INNER JOIN products_variants_master pvm ON pm.id = pvm.products_master_id WHERE pvm.variant_sku like '%".$searchFor."%') as a ORDER BY product_title");
		
		$stmt->execute();
		
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($products_master_id, $product_id, $product_title, $product_handle, $product_image, $product_orders_count);

			while($stmt->fetch()){
				$innerArray = array();
				$innerArray["products_master_id"] = $products_master_id;
                $innerArray["product_id"] = $product_id;
                $innerArray["product_title"] = $product_title;
                $innerArray["product_handle"] = $product_handle;
				$innerArray["product_image"] = $product_image;
				$innerArray["product_orders_count"] = $product_orders_count;
				
				$resultArray[] = $innerArray;
			}
			$stmt->free_result();
		}
	
		$stmt->close();
		
		parent::disconnect($mysql);
		
		parent::sendJson($resultArray);
	}

	protected function exportProductOrders_f_mdl($productMasterId, $storeProductId, $productTitle)
	{
		$mysql = parent::connect();

		$returnFileName = "";

		$resultArray = array();

		$stmt = $mysql->prepare("SELECT om.id as order_master_id, om.order_id, om.order_no, om.order_customer_name, om.order_email, om.order_shipping_charge, om.is_order_free, om.is_order_fulfilled, olim.id as order_line_items_master_id, olim.line_item_id, olim.variant_id, olim.variant_title, olim.variant_sku, olim.variant_quantity, olim.fulfilled_quantity, (SELECT temp_1.product_title FROM products_master temp_1 WHERE temp_1.product_id = '1532652617826') as product_title, (SELECT temp_2.money_format FROM shop_management temp_2 WHERE id = 1) as store_currency FROM orders_master om INNER JOIN orders_line_items_master olim ON om.id = olim.orders_master_id WHERE olim.variant_product_id = '".$storeProductId."' ORDER BY om.order_no");

		$stmt->execute();
	
		$stmt->store_result();

		if($stmt->num_rows > 0){
			$stmt->bind_result($order_master_id, $order_id, $order_no, $order_customer_name, $order_email, $order_shipping_charge, $is_order_free, $is_order_fulfilled, $order_line_items_master_id, $line_item_id, $variant_id, $variant_title, $variant_sku, $variant_quantity, $fulfilled_quantity, $product_title, $store_currency);

			#region - Create CSV File Start
			$delimiter = ",";
			$filename = str_replace(' ','_', str_replace('-','_', $productTitle))."_".date('Y_m_d_H_i_s') . ".csv";
			$returnFileName = $filename;

			$f = fopen(common::ORDER_EXPORT_DIR.$filename, 'w');
			
			$fields = array('Order No', 'Order ID', 'Customer Name', 'Customer Email', 'Shipping Charge', 'Order Free/Paid Status', 'Order Fulfillment Status', 'Variant Title', 'Variant SKU', 'Variant Quantity', 'Fulfilled Quantity');
			fputcsv($f, $fields, $delimiter);

			while($stmt->fetch()){
				$lineData = array($order_no, $order_id, $order_customer_name, $order_email, str_replace('{{amount}}', $order_shipping_charge, $store_currency), ($is_order_free == 1?"Free Shipping":"Paid Shipping"), ($is_order_fulfilled == 1?"Order Fulfilled":"Unfulfilled"), $variant_title, $variant_sku, $variant_quantity, $fulfilled_quantity);

				fputcsv($f, $lineData, $delimiter);
			}

			fseek($f, 0);

			fpassthru($f);
			#endregion

			$stmt->free_result();
		}
	
		$stmt->close();

		parent::disconnect($mysql);

		return common::ORDER_EXPORT_DIR.$returnFileName;
	}
}
?>
