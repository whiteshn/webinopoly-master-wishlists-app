<?php
include_once 'model/dashboard_mdl.php';

class dashboard_ctl extends dashboard_mdl
{
	function __construct(){	
		if(parent::isGET() || parent::isPOST()){
			$this->SITE_ACCESS_KEY = parent::getVal("stkn");
		}
		
		common::CheckLoginSession();
	}
}
?>