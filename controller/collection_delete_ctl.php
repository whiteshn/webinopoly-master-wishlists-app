<?php
include_once 'model/collection_delete_mdl.php';

class collection_delete_ctl extends collection_delete_mdl
{
	function __construct(){
		$this->verify_process();
	}

	function verify_process(){
        if(isset($_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'])){
            $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        
            $data = file_get_contents('php://input');
            
            $verified = $this->verify_webhook($data, $hmac_header);
            
            if($verified)
            {
                $varStoreName = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
                
                $decodeJsonCollectionInfo = json_decode($data);

                #region - Set Collection Variables
                $collectionId = $decodeJsonCollectionInfo->id;
                $collectionTitle = $decodeJsonCollectionInfo->title;
                $collectionHandle = $decodeJsonCollectionInfo->handle;
                #endregion

                #region - Get Collection Info
                $currentCollectionInfo = parent::getCollectionInfo_f_mdl($collectionId);
                #endregion

                if(count($currentCollectionInfo) > 0){
                    $masterCollectionId = $currentCollectionInfo[0]["id"];

                    #region - Remove Collection From DB
                    parent::removeCollectionFromDB_f_mdl($masterCollectionId);
                    #endregion

                    #region - Remove Collection From Product Belongs Table
                    parent::removeCollectionFromProductBelongTBL_f_mdl($masterCollectionId);
                    #endregion
                }
            }
        }
	}

	function verify_webhook($data, $hmac_header){
		$calculated_hmac = base64_encode(hash_hmac('sha256', $data, common::SHOPIFY_SECRET, true));
		return ($hmac_header == $calculated_hmac);
    }
}
?>
