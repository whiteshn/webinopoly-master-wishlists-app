<?php
include_once 'model/fetch_store_all_products_script_mdl.php';

class fetch_store_all_products_script_ctl extends fetch_store_all_products_script_mdl
{
	function __construct(){
		if(common::isGET()){
			$token = common::getVal("token");
			
			if($token == common::CRN_TOKEN && false){
				$this->fetchStoreAllProducts();
			}
			else{
				echo "Invalid token";
			}
		}
		else{
			echo "Not authorize to process";
		}
    }
    
    function fetchStoreAllProducts(){

		#region - Remove All Products & Variants From DB Table
		parent::removeProductsInfoFromDB_f_mdl();
		#endregion

        #region - Fetch Store Info
        $storeInfo = parent::getStoreInfo_f_mdl();
        #endregion

        if(count($storeInfo) > 0){
            require_once('lib/shopify.php');

            $shopifyObject = new ShopifyClient($storeInfo[0]["shop_name"], $storeInfo[0]["token"], common::SHOPIFY_API_KEY, common::SHOPIFY_SECRET);

            #region - Get Store All Products
            $isFoundProducts = true;
            $currentPage = 1;
			$masterProductsArray = array();

            do{
                try
                {
                    $storeProductsInfo = $shopifyObject->call('GET', '/admin/products.json?fields=id,handle,title,variants,images&limit=250&page='.$currentPage);

                    $encodeJsonProductsInfo = json_encode($storeProductsInfo);
                    $decodeJsonProductsInfo = json_decode($encodeJsonProductsInfo);

					#region - Add Particular Response To Specific Array
					if(count($decodeJsonProductsInfo) > 0){
						$currentPage++;

						foreach($decodeJsonProductsInfo as $objProduct){
							$innerArray = array();
							$innerArray["store_product_id"] = $objProduct->id;
							$innerArray["store_product_title"] = $objProduct->title;
							$innerArray["store_product_handle"] = $objProduct->handle;
							$innerArray["store_product_variants"] = $objProduct->variants;
                            $innerArray["store_product_images"] = $objProduct->images;
                            
							if(isset($objProduct->images[0])){
								$innerArray["store_product_feature_img"] = $objProduct->images[0]->src;
							}
							else{
								$innerArray["store_product_feature_img"] = "";
							}

							$masterProductsArray[] = $innerArray;
						}

						sleep(1);
					}
					else{
						$isFoundProducts = false;
					}
					#endregion
                }
                catch (ShopifyApiException $e)
                {   
                }
                catch (ShopifyCurlException $e)
                {   
                }
            }
            while($isFoundProducts);
			#endregion
			
			#region - Add Products To DB
			if(count($masterProductsArray) > 0){
				foreach($masterProductsArray as $objProductInfo){
					$store_product_id = $objProductInfo["store_product_id"];
					$store_product_title = $objProductInfo["store_product_title"];
					$store_product_handle = $objProductInfo["store_product_handle"];
					$store_product_feature_img = $objProductInfo["store_product_feature_img"];

					#region - Add Product To DB
					$productMasterId = parent::addMasterProduct_f_mdl($store_product_id, $store_product_title, $store_product_handle, $store_product_feature_img);
					#endregion

					#region - Add Products Variants
					$variantsBulkIns = "";
					if(count($objProductInfo["store_product_variants"] > 0)){
						foreach($objProductInfo["store_product_variants"] as $objVariantInfo){
                            $tempImgId = $objVariantInfo->image_id;
                            $tempImg = "";

                            #region - Find Variant Image
                            if(!empty($tempImgId)){
                                if(count($objProductInfo["store_product_images"]) > 0){
                                    foreach($objProductInfo["store_product_images"] as $objImgInfo){
                                        if($objImgInfo->id == $tempImgId){
                                            $tempImg = $objImgInfo->src;
                                        }
                                    }
                                }
                            }
                            else{
                                $tempImg =  $store_product_feature_img;
                            }
                            #endregion

							$variantsBulkIns .= "(".$productMasterId.", '".$objVariantInfo->id."', '".$objVariantInfo->title."', '".$objVariantInfo->sku."', '".$tempImg."', ".$objVariantInfo->price.", now()),";
						}
					}

					$variantsBulkIns = trim($variantsBulkIns, ",");

					parent::bulkProductVariantsInsert_f_mdl($variantsBulkIns);
					#endregion
				}
			}
			#endregion
        }
    }
}
?>