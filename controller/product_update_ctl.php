<?php
include_once 'model/product_update_mdl.php';

class product_update_ctl extends product_update_mdl
{
	function __construct(){
		$this->verify_process();
	}

	function verify_process(){
        if(isset($_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'])){
            $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        
            $data = file_get_contents('php://input');
            
            $verified = $this->verify_webhook($data, $hmac_header);
            
            if($verified)
            {
                $varStoreName = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
                
                $decodeJsonProductsInfo = json_decode($data);

                $masterProductsArray = array();

                #region - Fetch Product Info
                $innerArray = array();
                $innerArray["store_product_id"] = $decodeJsonProductsInfo->id;
                $innerArray["store_product_title"] = $decodeJsonProductsInfo->title;
                $innerArray["store_product_handle"] = $decodeJsonProductsInfo->handle;
                $innerArray["store_product_variants"] = $decodeJsonProductsInfo->variants;
                $innerArray["store_product_images"] = $decodeJsonProductsInfo->images;
                
                if(isset($decodeJsonProductsInfo->images[0])){
                    $innerArray["store_product_feature_img"] = $decodeJsonProductsInfo->images[0]->src;
                }
                else{
                    $innerArray["store_product_feature_img"] = "";
                }

                $masterProductsArray[] = $innerArray;
                #endregion

                #region - Add Products To DB
                if(count($masterProductsArray) > 0){
                    $productMasterId = 0;
                    foreach($masterProductsArray as $objProductInfo){
                        $store_product_id = $objProductInfo["store_product_id"];
                        $store_product_title = $objProductInfo["store_product_title"];
                        $store_product_handle = $objProductInfo["store_product_handle"];
                        $store_product_feature_img = $objProductInfo["store_product_feature_img"];

                        #region - Add Product To DB
                        parent::updateMasterProduct_f_mdl($store_product_id, $store_product_title, $store_product_handle, $store_product_feature_img);
                        #endregion

                        #region - Get Master Product Id & Delete Its Variants
                        $productMasterId = parent::getMasterProductIdAndDeleteVariants_f_mdl($store_product_id);
                        #endregion

                        #region - Add Products Variants
                        $variantsBulkIns = "";
                        if(count($objProductInfo["store_product_variants"] > 0)){
                            foreach($objProductInfo["store_product_variants"] as $objVariantInfo){
                                $tempImgId = $objVariantInfo->image_id;
                                $tempImg = "";

                                #region - Find Variant Image
                                if(!empty($tempImgId)){
                                    if(count($objProductInfo["store_product_images"]) > 0){
                                        foreach($objProductInfo["store_product_images"] as $objImgInfo){
                                            if($objImgInfo->id == $tempImgId){
                                                $tempImg = $objImgInfo->src;
                                            }
                                        }
                                    }
                                }
                                else{
                                    $tempImg =  $store_product_feature_img;
                                }
                                #endregion

                                /*$variantsBulkIns .= "(".$productMasterId.", '".$objVariantInfo->id."', '".$objVariantInfo->title."', '".$objVariantInfo->sku."', '".$tempImg."', ".$objVariantInfo->price.", now()),";*/

                                #region - Update Product Variants Info
                                parent::updateProductVariantsInfo_f_mdl($productMasterId, $objVariantInfo->id, $objVariantInfo->title, $objVariantInfo->sku, $tempImg, $objVariantInfo->price);
                                #endregion
                            }
                        }

                        /*$variantsBulkIns = trim($variantsBulkIns, ",");

                        parent::bulkProductVariantsInsert_f_mdl($variantsBulkIns);*/
                        #endregion
                    }

                    #region - Check Product Belongs From Collections
                    $this->getProductBelongsCollections($decodeJsonProductsInfo->id, $productMasterId);
                    #endregion
                }
                #endregion
            }
        }
    }
    
	function verify_webhook($data, $hmac_header){
		$calculated_hmac = base64_encode(hash_hmac('sha256', $data, common::SHOPIFY_SECRET, true));
		return ($hmac_header == $calculated_hmac);
    }
    
    function getProductBelongsCollections($storeProductId, $dbMasterProductId){
        if($storeProductId != "" && $dbMasterProductId > 0){
            #region - Remove Product Belongs Collections Rows
            parent::removeProductBelongsCollectionsRows_f_mdl($dbMasterProductId);
            #endregion

            #region - Fetch Store Info
            $storeInfo = parent::getStoreInfo_f_mdl();
            #endregion

            if(count($storeInfo) > 0){
                #region - Get Store All Collections
                $storeAllCollections = parent::getStoreAllCollections_f_mdl();
                #endregion

                if(count($storeAllCollections) > 0){
                    require_once('lib/shopify.php');

                    #region - Shopify Class Object
                    $shopifyObject = new ShopifyClient($storeInfo[0]["shop_name"], $storeInfo[0]["token"], common::SHOPIFY_API_KEY, common::SHOPIFY_SECRET);
                    #endregion

                    $masterProductsArray = array();
                    
                    try
                    {
                        $collectionsInfo = $shopifyObject->call('GET', 'admin/collects.json?product_id='.$storeProductId.'&limit=250');

                        $encodeJsonCollectionInfo = json_encode($collectionsInfo);
                        $decodeJsonCollectionInfo = json_decode($encodeJsonCollectionInfo);

                        #region - Add Particular Response To Specific Array
                        if(count($decodeJsonCollectionInfo) > 0){
                            foreach($decodeJsonCollectionInfo as $objCollection){
                                $innerArray = array();
                                $innerArray["product_id"] = $storeProductId;
                                $innerArray["collection_id"] = $objCollection->collection_id;
                                
                                $masterProductsArray[] = $innerArray;
                            }
                        }
                        #endregion
                    }
                    catch (ShopifyApiException $e)
                    {
                    }
                    catch (ShopifyCurlException $e)
                    {
                    }
                    
                    if(count($masterProductsArray) > 0){
                        #region - Get DB Collection Ids & Product Ids
                        $finalDBInsertArray = $this->filterAndGetCollectionProductIds($storeAllCollections, $dbMasterProductId, $masterProductsArray);
                        #endregion
                        
                        if(count($finalDBInsertArray) > 0){
                            $bulkInsertStr = "";
    
                            #region - Loop & Make Insert String
                            foreach($finalDBInsertArray as $objInfo){
                                $bulkInsertStr .= "(".$objInfo["master_collection_id"].", ".$objInfo["master_product_id"].", now()),";
                            }
                            #endregion
                            
                            if($bulkInsertStr != ""){
                                parent::insertBulkProductBelongsCollections_f_mdl(trim($bulkInsertStr, ","));
                            }
                        }
                    }
                }
            }
        }
    }

    function filterAndGetCollectionProductIds($storeAllCollections, $dbMasterProductId, $masterProductsArray){
        $returnArray = array();
        
        if(count($storeAllCollections) > 0 && count($masterProductsArray) > 0){
            foreach($masterProductsArray as $objColProd){
                $tempMasterCollectionId = 0;
                $tempMasterProductId = $dbMasterProductId;

                $loopCollectionId = $objColProd["collection_id"];
                $loopProductId = $objColProd["product_id"];

                #region - Get DB Collection Id
                foreach($storeAllCollections as $objTmpCollection){
                    if($objTmpCollection["store_collection_id"] == $loopCollectionId){
                        $tempMasterCollectionId = $objTmpCollection["master_collection_id"];
                    }
                }
                #endregion

                if($tempMasterCollectionId != 0 && $tempMasterProductId != 0){
                    $tempInnerArray = array();
                    $tempInnerArray["master_collection_id"] = $tempMasterCollectionId;
                    $tempInnerArray["master_product_id"] = $tempMasterProductId;

                    $returnArray[] = $tempInnerArray;
                }
            }
        }

        return $returnArray;
    }
}
?>
