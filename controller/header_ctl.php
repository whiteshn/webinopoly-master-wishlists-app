<?php
include_once 'model/header_mdl.php';

class header_ctl extends header_mdl
{
	function __construct()
	{
		if(parent::isGET() || parent::isPOST()){
			$this->SITE_ACCESS_KEY = parent::getVal("stkn");
		}

		common::CheckLoginSession();
	}
}
?>