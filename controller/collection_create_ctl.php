<?php
include_once 'model/collection_create_mdl.php';

class collection_create_ctl extends collection_create_mdl
{
	function __construct(){
		$this->verify_process();
	}

	function verify_process(){
        if(isset($_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'])){
            $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        
            $data = file_get_contents('php://input');
            
            $verified = $this->verify_webhook($data, $hmac_header);
            
            if($verified)
            {
                $varStoreName = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
                
                $decodeJsonCollectionInfo = json_decode($data);

                #region - Set Collection Variables
                $collectionId = $decodeJsonCollectionInfo->id;
                $collectionTitle = $decodeJsonCollectionInfo->title;
                $collectionHandle = $decodeJsonCollectionInfo->handle;
                #endregion

                #region - Add New Collection To DB
                $masterCollectionId = parent::addNewCollection_f_mdl($collectionId, $collectionTitle, $collectionHandle);
                #endregion

                if($masterCollectionId > 0){
                    #region - Fetch Store Info
                    $storeInfo = parent::getStoreInfo_f_mdl();
                    #endregion

                    #region - Get Store All Products
                    $storeAllProducts = parent::getStoreAllProducts_f_mdl();
                    #endregion

                    require_once('lib/shopify.php');

                    #region - Shopify Class Object
                    $shopifyObject = new ShopifyClient($storeInfo[0]["shop_name"], $storeInfo[0]["token"], common::SHOPIFY_API_KEY, common::SHOPIFY_SECRET);
                    #endregion

                    $masterProductsArray = array();

                    #region - Get Particular Collection's All Products
                    $isFoundProducts = true;
                    $currentPage = 1;

                    do{
                        try
                        {
                            $collectionProductsInfo = $shopifyObject->call('GET', '/admin/products.json?collection_id='.$collectionId.'&limit=250&page='.$currentPage);

                            $encodeJsonCollectionProductsInfo = json_encode($collectionProductsInfo);
                            $decodeJsonCollectionProductsInfo = json_decode($encodeJsonCollectionProductsInfo);

                            #region - Add Particular Response To Specific Array
                            if(count($decodeJsonCollectionProductsInfo) > 0){
                                $currentPage++;

                                foreach($decodeJsonCollectionProductsInfo as $objProduct){
                                    $innerArray = array();
                                    $innerArray["product_id"] = $objProduct->id;
                                    $innerArray["collection_id"] = $collectionId;
                                    
                                    $masterProductsArray[] = $innerArray;
                                }

                                sleep(0.5);
                            }
                            else{
                                $isFoundProducts = false;
                            }
                            #endregion
                        }
                        catch (ShopifyApiException $e)
                        {
                            print_r($e);
                            exit;
                        }
                        catch (ShopifyCurlException $e)
                        {
                            print_r($e);
                            exit;
                        }
                    }
                    while($isFoundProducts);
                    #endregion

                    if(count($masterProductsArray) > 0){
                        #region - Get DB Collection Ids & Product Ids
                        $finalDBInsertArray = $this->filterAndGetCollectionProductIds($masterCollectionId, $storeAllProducts, $masterProductsArray);
                        #endregion

                        if(count($finalDBInsertArray) > 0){
                            $bulkInsertStr = "";
    
                            #region - Loop & Make Insert String
                            foreach($finalDBInsertArray as $objInfo){
                                $bulkInsertStr .= "(".$objInfo["master_collection_id"].", ".$objInfo["master_product_id"].", now()),";
                            }
                            #endregion
                            
                            if($bulkInsertStr != ""){
                                parent::insertBulkProductBelongsCollections_f_mdl(trim($bulkInsertStr, ","));
                            }
                        }
                    }
                }
            }
        }
	}

	function verify_webhook($data, $hmac_header){
		$calculated_hmac = base64_encode(hash_hmac('sha256', $data, common::SHOPIFY_SECRET, true));
		return ($hmac_header == $calculated_hmac);
    }
    
    function filterAndGetCollectionProductIds($masterCollectionId, $storeAllProducts, $masterProductsArray){
        $returnArray = array();

        if($masterCollectionId > 0 && count($storeAllProducts) > 0 && count($masterProductsArray) > 0){
            foreach($masterProductsArray as $objColProd){
                $tempMasterCollectionId = $masterCollectionId;
                $tempMasterProductId = 0;

                $loopCollectionId = $objColProd["collection_id"];
                $loopProductId = $objColProd["product_id"];
                
                #region - Get DB Product Id
                foreach($storeAllProducts as $objTmpProduct){
                    if($objTmpProduct["store_product_id"] == $loopProductId){
                        $tempMasterProductId = $objTmpProduct["master_product_id"];
                    }
                }
                #endregion

                if($tempMasterCollectionId != 0 && $tempMasterProductId != 0){
                    $tempInnerArray = array();
                    $tempInnerArray["master_collection_id"] = $tempMasterCollectionId;
                    $tempInnerArray["master_product_id"] = $tempMasterProductId;

                    $returnArray[] = $tempInnerArray;
                }
            }
        }

        return $returnArray;
    }
}
?>
