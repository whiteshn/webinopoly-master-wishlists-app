<?php
include_once 'model/set_store_products_collections_mdl.php';

class set_store_products_collections_ctl extends set_store_products_collections_mdl
{
	function __construct(){
		if(common::isGET()){
			$token = common::getVal("token");
			
			if($token == common::CRN_TOKEN && false){
				$this->setStoreProductsBelongsCollections();
			}
			else{
				echo "Invalid token";
			}
		}
		else{
			echo "Not authorize to process";
		}
    }
    
    function setStoreProductsBelongsCollections(){

		#region - Remove All Products & Variants From DB Table
		parent::removeProductsInfoFromDB_f_mdl();
		#endregion

        #region - Fetch Store Info
        $storeInfo = parent::getStoreInfo_f_mdl();
        #endregion

        if(count($storeInfo) > 0){

            #region - Get Store All Collections
            $storeAllCollections = parent::getStoreAllCollections_f_mdl();
            #endregion

            if(count($storeAllCollections) > 0){

                #region - Get Store All Products
                $storeAllProducts = parent::getStoreAllProducts_f_mdl();
                #endregion

                require_once('lib/shopify.php');

                #region - Shopify Class Object
                $shopifyObject = new ShopifyClient($storeInfo[0]["shop_name"], $storeInfo[0]["token"], common::SHOPIFY_API_KEY, common::SHOPIFY_SECRET);
                #endregion

                $masterProductsArray = array();

                foreach($storeAllCollections as $objCollection){
                    $tmpCollectionId = $objCollection["store_collection_id"];

                    #region - Get Particular Collection's All Products
                    $isFoundProducts = true;
                    $currentPage = 1;

                    do{
                        try
                        {
                            $collectionProductsInfo = $shopifyObject->call('GET', '/admin/products.json?collection_id='.$tmpCollectionId.'&limit=250&page='.$currentPage);

                            $encodeJsonCollectionProductsInfo = json_encode($collectionProductsInfo);
                            $decodeJsonCollectionProductsInfo = json_decode($encodeJsonCollectionProductsInfo);

                            #region - Add Particular Response To Specific Array
                            if(count($decodeJsonCollectionProductsInfo) > 0){
                                $currentPage++;

                                foreach($decodeJsonCollectionProductsInfo as $objProduct){
                                    $innerArray = array();
                                    $innerArray["product_id"] = $objProduct->id;
                                    $innerArray["collection_id"] = $tmpCollectionId;
                                    
                                    $masterProductsArray[] = $innerArray;
                                }

                                sleep(0.5);
                            }
                            else{
                                $isFoundProducts = false;
                            }
                            #endregion
                        }
                        catch (ShopifyApiException $e)
                        {
                            print_r($e);
                            exit;
                        }
                        catch (ShopifyCurlException $e)
                        {
                            print_r($e);
                            exit;
                        }
                    }
                    while($isFoundProducts);
                    #endregion
                }

                /*print_r(json_encode($masterProductsArray));
                exit;*/

                if(count($masterProductsArray) > 0){
                    #region - Get DB Collection Ids & Product Ids
                    $finalDBInsertArray = $this->filterAndGetCollectionProductIds($storeAllCollections, $storeAllProducts, $masterProductsArray);
                    #endregion

                    if(count($finalDBInsertArray) > 0){
                        $bulkInsertStr = "";

                        #region - Loop & Make Insert String
                        foreach($finalDBInsertArray as $objInfo){
                            $bulkInsertStr .= "(".$objInfo["master_collection_id"].", ".$objInfo["master_product_id"].", now()),";
                        }
                        #endregion

                        if($bulkInsertStr != ""){
                            parent::insertBulkProductBelongsCollections_f_mdl(trim($bulkInsertStr, ","));
                        }
                    }
                }
            }
        }
    }

    function filterAndGetCollectionProductIds($storeAllCollections, $storeAllProducts, $masterProductsArray){
        $returnArray = array();

        if(count($storeAllCollections) > 0 && count($storeAllProducts) > 0 && count($masterProductsArray) > 0){
            foreach($masterProductsArray as $objColProd){
                $tempMasterCollectionId = 0;
                $tempMasterProductId = 0;

                $loopCollectionId = $objColProd["collection_id"];
                $loopProductId = $objColProd["product_id"];
                
                #region - Get DB Collection Id
                foreach($storeAllCollections as $objTmpCollection){
                    if($objTmpCollection["store_collection_id"] == $loopCollectionId){
                        $tempMasterCollectionId = $objTmpCollection["master_collection_id"];
                    }
                }
                #endregion

                #region - Get DB Product Id
                foreach($storeAllProducts as $objTmpProduct){
                    if($objTmpProduct["store_product_id"] == $loopProductId){
                        $tempMasterProductId = $objTmpProduct["master_product_id"];
                    }
                }
                #endregion

                if($tempMasterCollectionId != 0 && $tempMasterProductId != 0){
                    $tempInnerArray = array();
                    $tempInnerArray["master_collection_id"] = $tempMasterCollectionId;
                    $tempInnerArray["master_product_id"] = $tempMasterProductId;

                    $returnArray[] = $tempInnerArray;
                }
            }
        }

        return $returnArray;
    }
}
?>