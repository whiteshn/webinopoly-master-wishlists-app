<?php
include_once 'model/index_mdl.php';

class index_ctl extends index_mdl
{
	public $TempSession = "";

	function index_ctl(){
		$this->TempSession = $this->setSiteAccessKey();
		
		common::CheckLoginSession();	
	}
	
	function setSiteAccessKey(){
		
		$ReturnSessionName = "";

		#region - GET STORE ACCESS KEY
		if(parent::isGET() || parent::isPOST()){
			if(parent::getVal("shop") != ""){
				$this->shop_name = parent::getVal("shop");
				$StoreDetail = parent::getShopDetail_f_mdl();
				
				if(count($StoreDetail) > 0){
					$ReturnSessionName = common::STORE_LOGIN_SESSION_FIRST_KEY.$StoreDetail[0]["site_access_key"];
					$this->SITE_ACCESS_KEY = $StoreDetail[0]["site_access_key"];
				}
			}
		}
		#endregion
		
		return $ReturnSessionName;
	}
	
	function redirect_to_dashboard()
	{
		if(isset($_SESSION[$this->TempSession]))
		{
			header("location: ".common::SITE_URL."dashboard.php?stkn=".$this->SITE_ACCESS_KEY);
		}
		else
		{
			header("location: ".common::SITE_URL."login.php?shop=".$this->shop_name);
		}
		exit;
	}
}
?>