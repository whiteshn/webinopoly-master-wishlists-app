<?php
include_once 'model/fetch_store_all_collections_script_mdl.php';

class fetch_store_all_collections_script_ctl extends fetch_store_all_collections_script_mdl
{
	function __construct(){
		if(common::isGET()){
			$token = common::getVal("token");
			
			if($token == common::CRN_TOKEN && false){
				$this->fetchStoreAllCollections();
			}
			else{
				echo "Invalid token";
			}
		}
		else{
			echo "Not authorize to process";
		}
    }
    
    function fetchStoreAllCollections(){

		#region - Fetch Store Info
        $storeInfo = parent::getStoreInfo_f_mdl();
        #endregion

        if(count($storeInfo) > 0){
            require_once('lib/shopify.php');

            $shopifyObject = new ShopifyClient($storeInfo[0]["shop_name"], $storeInfo[0]["token"], common::SHOPIFY_API_KEY, common::SHOPIFY_SECRET);

            $masterCollectionsArray = array();

            #region - Get Store Custom Collections
            try
            {
                $customCollectionsInfo = $shopifyObject->call('GET', '/admin/custom_collections.json?limit=250');

                $encodeJsonCustomColInfo = json_encode($customCollectionsInfo);
                $decodeJsonCustomColInfo = json_decode($encodeJsonCustomColInfo);

                #region - Add All Custom Collections To Master Array
                if(count($decodeJsonCustomColInfo) > 0){
                    foreach($decodeJsonCustomColInfo as $objCustomCol){
                        $innerArray = array();
                        $innerArray["store_collection_id"] = $objCustomCol->id;
                        $innerArray["store_collection_name"] = $objCustomCol->title;
                        $innerArray["store_collection_handle"] = $objCustomCol->handle;
                        
                        $masterCollectionsArray[] = $innerArray;
                    }
                }
                #endregion
            }
            catch (ShopifyApiException $e)
            {                
            }
            catch (ShopifyCurlException $e)
            {                
            }
            #endregion

            #region - Get Store Smart Collections
            try
            {
                $smartCollectionsInfo = $shopifyObject->call('GET', '/admin/smart_collections.json?limit=250');

                $encodeJsonSmartColInfo = json_encode($smartCollectionsInfo);
                $decodeJsonSmartColInfo = json_decode($encodeJsonSmartColInfo);

                #region - Add All Custom Collections To Master Array
                if(count($decodeJsonSmartColInfo) > 0){
                    foreach($decodeJsonSmartColInfo as $objSmartCol){
                        $innerArray = array();
                        $innerArray["store_collection_id"] = $objSmartCol->id;
                        $innerArray["store_collection_name"] = $objSmartCol->title;
                        $innerArray["store_collection_handle"] = $objSmartCol->handle;
                        
                        $masterCollectionsArray[] = $innerArray;
                    }
                }
                #endregion
            }
            catch (ShopifyApiException $e)
            {                
            }
            catch (ShopifyCurlException $e)
            {                
            }
            #endregion

            #region - Add Collections Into DB
            if(count($masterCollectionsArray) > 0){
                $bulkInsertStr = "";

                #region - Create Bulk Insert String
                foreach($masterCollectionsArray as $objCol){
                    $bulkInsertStr .= "('".$objCol["store_collection_id"]."', '".$objCol["store_collection_name"]."', '".$objCol["store_collection_handle"]."', now()),";
                }
                #endregion

                if($bulkInsertStr != ""){
                    parent::addStoreCollections_f_mdl(trim($bulkInsertStr, ","));
                }
            }
            #endregion
        }
    }
}
?>