<?php
include_once 'model/product_delete_mdl.php';

class product_delete_ctl extends product_delete_mdl
{
	function __construct(){
		$this->verify_process();
	}

	function verify_process(){
        if(isset($_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'])){
            $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
        
            $data = file_get_contents('php://input');
            
            $verified = $this->verify_webhook($data, $hmac_header);
            
            if($verified)
            {
                $varStoreName = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
                
                $decodeJsonProductsInfo = json_decode($data);

                #region - Fetch Product Info
                $shopifyProductId = $decodeJsonProductsInfo->id;
                #endregion

                #region - Get Product Info From DB
                $productDBInfo = parent::getProductDBInfo_f_mdl($shopifyProductId);
                #endregion

                if(count($productDBInfo) > 0){
                    $masterProductId = $productDBInfo[0]["id"];

                    #region - Delete Master Product & Its Variants
                    parent::deleteProductFromEverywhere_f_mdl($masterProductId);
                    #endregion
                }
            }
        }
    }
    
	function verify_webhook($data, $hmac_header)
	{
		$calculated_hmac = base64_encode(hash_hmac('sha256', $data, common::SHOPIFY_SECRET, true));
		return ($hmac_header == $calculated_hmac);
	}
}
?>
