<?php
include_once 'model/process_on_request_mdl.php';

class process_on_request_ctl extends process_on_request_mdl
{
	function __construct(){	
		if(parent::isGET() || parent::isPOST()){
            if(!empty(parent::getVal("rfor"))){
                $this->checkRequestProcess(parent::getVal("rfor"));
            }
            else{
                header("HTTP/1.0 404 Not Found");
                exit;
            }
		}
    }
    
    function checkRequestProcess($requestFor){
        if($requestFor != ""){
            switch($requestFor){
                case "get-cust-items":
                    $this->getCustomerWihlistItems();
                break;
                case "add-whislist-prod":
                    $this->addWishlistProduct();
                break;
                case "get-cust-items-for-list":
                    $this->getCustomerItemsForList();
                break;
                case "rem-from-list":
                    $this->removeProductFromWishist();
                break;
                case "get-collect-wise-items":
                    $this->getCollectionWiseItems();
                break;
            }
        }
    }

    function getCustomerWihlistItems(){
        #region - Set Variables
        $storeCustomerId = parent::getVal("cid");
        $this->customer_id = $storeCustomerId;
        #endregion

        #region - Get Customer Selected Products
        parent::getCustomerSelectedProductsShortInfo_f_mdl();
        #endregion
    }

    function addWishlistProduct(){
        #region - Set Variables
        $this->customer_id = parent::getVal("cid");
        $this->store_product_id = parent::getVal("pid");
        #endregion

        #region - Get Master Product Id From DB
        $productInfo = parent::getMasterProductInfo_f_mdl();
        #endregion

        if(count($productInfo) > 0){
            $bulkInsStr = "";
            foreach($productInfo as $objProd){
                $bulkInsStr .= "('".$this->customer_id."', ".$objProd["master_products_id"].", ".$objProd["master_products_variants_id"].", now()),";
            }
            
            if($bulkInsStr != ""){
                #region - Add Current Product To Wishlist Item
                parent::addCurrentProductToWishlistItem_f_mdl(trim($bulkInsStr, ","));
                #endregion
            }
        }
    }

    function getCustomerItemsForList(){
        #region - Set Variables
        $this->customer_id = parent::getVal("cid");
        #endregion

        #region - Get Customer Wishlist's Products
        parent::getCustomerItemsForList_f_mdl();
        #endregion
    }

    function removeProductFromWishist(){
        #region - Set Variables
        $this->customer_id = parent::getVal("cid");
        $this->customer_selected_master_id = parent::getVal("cmid");
        #endregion

        parent::removeProductFromWishist_f_mld();
    }

    function getCollectionWiseItems(){
        #region - Set Variables
        $this->customer_id = parent::getVal("cid");
        $requestCollectionId = parent::getVal("collectid");
        #endregion

        if($requestCollectionId == "1"){
            #region - Get Customer Wishlist's Products
            parent::getCustomerItemsForList_f_mdl();
            #endregion
        }
        else{
            #region - Get Collection Master Id
            $collectionInfo = parent::getCollectionMasterInfo_f_mdl($requestCollectionId);
            #endregion

            if(count($collectionInfo) > 0){
                $this->master_collections_id = $collectionInfo[0]["id"];

                #region - Get Products Collection Wise
                parent::getCustomerItemsForListCollectWise_f_mdl();
                #endregion
            }
        }
    }
}
?>