<?php include_once("model/common.php");

	session_start();
	ob_start();
	if(isset($_GET["stkn"]))
	{
		$sessionName = common::STORE_LOGIN_SESSION_FIRST_KEY.$_GET["stkn"];
		unset($_SESSION[$sessionName]);
	}
	else
	{
		session_unset(); 
		session_destroy();
	}
	header("location:/login.php");
	exit;
?>