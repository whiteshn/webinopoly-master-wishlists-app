export const ACTIVE = '1';
export const DEACTIVE = '0';
export const YES = 1;
export const NO = 0;
export const WHITE_URL_LOADER = 'image/loader-white.svg';
export const SKY_URL_LOADER = 'image/loader-sky.svg';
export const WHITE_LOADER_ACTIVE_CLASS = 'loader-white-active';
export const SKY_LOADER_ACTIVE_CLASS = 'loader-sky-active';
export const NO_IMAGE_PATH = 'image/no-image-found.png';
export const BACKSPACE = 'backspace';
export const DELETE = 'delete';
export const ENTER = 'enter';
export const PAGINATE_BY = 100;
export const ORDER_VIEW_URL = '/admin/orders/';

export const SITE_URL = "https://webinopoly.com";
export const SHOPIFY_EXPERT_LINK = "https://experts.shopify.com/webinopoly-com";

/* Page Routes Start */
export const DASHBOARD_PAGE_URL = 'dashboard.php';
/* Page Routes End */

/* Resource List Image Constants Start */
export const IMG = {
	THUMB_CROPPED_ONLY: '_thumb_cropped.',
	THUMB_CROPPED: {
		ICO: '_thumb_cropped.ico',
		GIF: '_thumb_cropped.gif',
		PNG: '_thumb_cropped.png',
		JPEG: '_thumb_cropped.jpeg',
		JPG: '_thumb_cropped.jpg'
	},
	THUMB: {
		ICO: '_60x.ico',
		GIF: '_60x.gif',
		PNG: '_60x.png',
		JPEG: '_60x.jpeg',
		JPG: '_60x.jpg'
	},
	ICO: '.ico',
	GIF: '.gif',
	PNG: '.png',
	JPEG: '.jpeg',
	JPG: '.jpg'
};
/* Resource List Image Constants End */