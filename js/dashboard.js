import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {
    Layout,
    Page,
    FooterHelp,
    Card,
    Link,
    Button,
    FormLayout,
    TextField,
    Checkbox,
    ResourceList,
    Thumbnail,
    Tag,
    Banner,
    TextStyle,
    Subheading,
    Select,
    KeyboardKey,
    Pagination,
    EmptyState
} from '@shopify/polaris';
import Header from './header';
import Footer from './footer';
import * as ALL_CONST from './all_constants';

class Dashboard extends Component {
	constructor(props) {
        super(props);
        
        this.state = {
        }
    }
	
	render() {
		return (
            <Page fullWidth={true}>
                <Layout>
                    <Layout.Section>
                        <Card title="Dashboard" sectioned>
                        <p>Data comes here...</p>
                        </Card>
                    </Layout.Section>
                </Layout>
            </Page>
		);
	}
}

export default Dashboard;