import React, {Component} from 'react';
import {
  Layout,
  FooterHelp,
  Link,
  Page,
  Card
} from '@shopify/polaris';
import * as ALL_CONST from './all_constants';

class Footer extends Component {
  constructor(props) {
    super(props);
  }

  render() {
	return (
		<div>
			<br />
			<Layout>
				<Layout.Section>
					<FooterHelp>App built by <Link url={ALL_CONST.SITE_URL} external>Webinopoly</Link>. We are <Link url={ALL_CONST.SHOPIFY_EXPERT_LINK} external>Shopify Experts</Link>.</FooterHelp>
				</Layout.Section>
			</Layout>
		</div>	
    );
  }
}

export default Footer;