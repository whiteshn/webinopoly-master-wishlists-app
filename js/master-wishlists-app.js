import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import * as ALL_CONST from './all_constants';
import Dashboard from './dashboard';

class MasterWishlistsApp extends Component {
  constructor(props) {
    super(props);
	
		var pageName = window.location.href.split("/")[window.location.href.split("/").length - 1];
		if(pageName.indexOf("?") > -1) pageName = pageName.split("?")[0];
		if(pageName.indexOf("#") > -1) pageName = pageName.split("#")[0];
	
		this.state = { 
			route: pageName
		};
  }
  
  render() {
		var Child;
		if(this.state.route.toLowerCase() == ALL_CONST.DASHBOARD_PAGE_URL)
			Child = Dashboard;
		
		return (
			<Child />
		);
  }
}

if(document.getElementById('root') !== null)
{
	ReactDOM.render(
		<MasterWishlistsApp />,
		document.getElementById('root')
	);
}