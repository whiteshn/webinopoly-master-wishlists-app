	<!-- Shopify Embedded Script Start -->
	<script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
	<!-- Shopify Embedded Script End -->
	<script>
		/* Shopify Embedded Code Start */
		ShopifyApp.init({
			apiKey: '<?php echo common::SHOPIFY_API_KEY; ?>',
			shopOrigin: 'https://demo-master-wishlist-app-store.myshopify.com',
			debug: false
		});
		ShopifyApp.ready(function(){
			ShopifyApp.Bar.loadingOff();
			
			var links = {};
			
			ShopifyApp.Bar.initialize({
				icon: '<?php echo common::IMG_URL.'jcg.png'; ?>',
				buttons: links
			});
		});
		/* Shopify Embedded Code End */
	</script>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js"></script>
	
	<script src="<?php echo common::BUNDLE_JS_URL; ?>"></script>
</body>
</html>