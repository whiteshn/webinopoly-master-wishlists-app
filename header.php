<?php 
	include 'controller/header_ctl.php';
	
	$objHeader = new header_ctl();
?>
<?php $version = common::GUID(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="manifest" href="/manifest.json">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  
	<title><?php echo common::SITE_NAME; ?></title>
  
  <link rel="stylesheet" href="https://sdks.shopifycdn.com/polaris/1.9.1/polaris.min.css" />
  <link rel="stylesheet" href="<?php echo common::APP_ADMIN_SHOPIFY_CDN_CSS_URL.$version; ?>" />
</head>
<body>