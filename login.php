<?php 
	include 'controller/login_ctl.php';
	
	$objLogin = new login_ctl();
	$objLogin->CheckLoginRequest();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php if($objLogin->authURL != "") { ?>
	<script type='text/javascript'>
		// If the current window is the 'parent', change the URL by setting location.href
		if (window.top == window.self) {
			window.top.location.href = "<?php echo $objLogin->authURL; ?>";
		// If the current window is the 'child', change the parent's URL with postMessage
		} else {
			message = JSON.stringify({
				message: "Shopify.API.remoteRedirect",
				data: { location: "<?php echo $objLogin->authURL; ?>" }
			});
			window.parent.postMessage(message, "https://<?php echo $objLogin->storeURL ?>");
		}
	</script>
	<?php } ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="shortcut icon" href="<?php echo common::SITE_URL ?>image/favicon.png?v=1" type="image/png" />
	
	<link href="css/bootstrap.min.css" rel="stylesheet">

	<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
	
    <link href="css/sb-admin.css?v=10" rel="stylesheet">

	<title>Login - <?php echo common::SITE_NAME; ?></title>
</head>
<body class="login">
	<?php if($objLogin->authURL != "") { ?>
		<div class="login-loader-container">
			<div class="login-loader-position">
				<div class="login-loader">
					<svg preserveAspectRatio="xMinYMin">
						<circle cx="50%" cy="50%" r="45%"></circle> 
					</svg>
				</div>
			</div>
		</div>
	<?php } 
	else { ?>
		<div class="logo">
			<img src="https://cdn.shopify.com/s/files/1/0028/8001/2353/files/logo_newcolors_d5b45b6b-8e69-4d7e-b897-a55ede1a74c4.png?14773489689717820668" alt="<?php echo common::SITE_NAME; ?>">
		</div>
		<div class="login-loader-position mt50px">
			<div class="login-loader">
				<svg preserveAspectRatio="xMinYMin">
					<circle cx="50%" cy="50%" r="45%"></circle> 
				</svg>
			</div>
		</div>
		<div class="content dn">
			<form class="login-form formValidator" method="post" action="login.php">
				<div class="form-group">
					<div class="input-icon">
						<i class="fa fa-user"></i>
						<input required class="form-control placeholder-no-fix valid" type="text" autocomplete="off" name="shop" id="shop" aria-required="true" aria-invalid="false" value="<?php echo common::PARENT_STORE_NAME; ?>">
					</div>
				</div>
				<div class="form-actions">
					<button id="submit_me" type="submit" class="btn green w100per">Login with Shopify</button>
				</div>
			</form>
		</div>
		<script>
			document.forms[0].submit();
		</script>
	<?php } ?>
</body>
</html>